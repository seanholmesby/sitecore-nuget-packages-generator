﻿


namespace Sitecore.NuGet.Core
{
  using System;
  using System.Collections.Generic;
  using System.Diagnostics;
  using System.IO;
  using System.Linq;
  using ICSharpCode.SharpZipLib.Zip;
  using Sitecore.Diagnostics.Base;
  using Sitecore.Diagnostics.Base.Annotations;
  using Sitecore.Diagnostics.InformationService.Client;
  using Sitecore.Diagnostics.InformationService.Client.Model;
  using ManifestDependency = global::NuGet.ManifestDependency;
  using ManifestDependencySet = global::NuGet.ManifestDependencySet;
  using ManifestMetadata = global::NuGet.ManifestMetadata;
  using PackageBuilder = global::NuGet.PackageBuilder;
  using PhysicalPackageFile = global::NuGet.PhysicalPackageFile;
  
  public class PackageGenerator
  {
    public const string InstallPs1 = @"param($installPath, $toolsPath, $package, $project)
$folder = $null
foreach($item in $project.ProjectItems)
{
  if($item.Name -eq ""App_Bin"")
  {
    $folder = $item;
  }
}

if($folder) 
{
}
else
{
  $folder = $project.ProjectItems.AddFolder(""App_Bin"")
}

$appBin = [System.IO.Path]::Combine($installPath, ""App_Bin"")
$files = Get-ChildItem $appBin
foreach($file in $files)
{
  foreach($item in $folder.ProjectItems)
  {
    if($item.Name -eq $file.Name)
    {
      $exists = $true
    }
  }
  
  if($exists)
  {
  }
  else
  {
    $folder.ProjectItems.AddFromFile($file.FullName)
  }
}";

    public const string UninstallPs1 = @"param($installPath, $toolsPath, $package, $project)
$folder = $null
foreach($item in $project.ProjectItems)
{
  if($item.Name -eq ""App_Bin"")
  {
    $folder = $item;
  }
}

if($folder) 
{
  $appBin = [System.IO.Path]::Combine($installPath, ""App_Bin"")
  $files = Get-ChildItem $appBin
  foreach($file in $files)
  {
    $remove = $null
    foreach($item in $folder.ProjectItems)
    {
      if($item.Name -eq $file.Name)
      {
        $remove = $item
      }
    }
    
    if($remove)
    {
      $remove.Remove()
    }
  }
}";

    public const string CopyLocalInstallPs1 = @"param($installPath, $toolsPath, $package, $project)
  
 write-host ===================================================
 write-host ""Setting 'CopyLocal' to false for the following references:""
  
 $asms = $package.AssemblyReferences | %{$_.Name}
  
 foreach ($reference in $project.Object.References)
 {
     if ($asms -contains $reference.Name + "".dll"")
     {
         Write-Host $reference.Name
         $reference.CopyLocal = $false;
     }
 }";

    [NotNull]
    public virtual IEnumerable<string> Generate([NotNull] string packageFilePath, 
                                                [NotNull] string outputFolderPath)
    {
        return Generate(packageFilePath, outputFolderPath, String.Empty, true);
    }


    [NotNull]
    public virtual IEnumerable<string> Generate([NotNull] string packageFilePath, 
                                                [NotNull] string outputFolderPath, 
                                                string userDefinedPackagePrefix,
                                                bool sitecoreOnlyDLLs)
    {
        Assert.ArgumentNotNull(packageFilePath, "packageFilePath");
        Assert.ArgumentNotNull(outputFolderPath, "outputFolderPath");

        var client = new ServiceClient();
        foreach (var productName in client.GetProductNames())
        {
            var filePrefix = GetFilePrefix(productName);
            var knownPackage = PackageGenerator.IsKnownPackage(productName);
            if (!knownPackage)
            {
                continue;
            }

            foreach (var version in client.GetVersions(productName))
            {
                var majorMinor = GetMajorMinor(version);
                foreach (var release in version.Releases)
                {
                    var revision = release.Revision;
                    var file = Path.GetFileName(packageFilePath);
                    if (!file.StartsWith(filePrefix))
                    {
                        continue;
                    }

                    if (!file.Contains(majorMinor))
                    {
                        continue;
                    }

                    if (!file.EndsWith(revision + ".zip"))
                    {
                        continue;

                    }
                    var releaseVersion = GetReleaseVersion(majorMinor, revision);
                    return Generate(userDefinedPackagePrefix, productName, packageFilePath, releaseVersion, outputFolderPath, sitecoreOnlyDLLs);
                }
            }
        }

        return new string[0];
    }


    public static string GetReleaseVersion(string majorMinor, string revision)
    {
        int dotCount = majorMinor.Count(c => c == '.');
        if (dotCount > 1)
        {
            return majorMinor + "." + revision;
        }

      return majorMinor + ".0." + revision;
    }

    public static string GetMajorMinor(IVersion version)
    {
      return version.Name.Replace(".0.0", ".0");
    }

    public static string GetAbbr(string productName)
    {
        return GetAbbr(productName, String.Empty);
    }

    public static string GetAbbr(string productName, string userDefinedPackagePrefix)
    {
        if (!IsKnownPackage(productName))
        {
            return null;
        }


        string packagePrefix = String.IsNullOrEmpty(userDefinedPackagePrefix) ? "Sitecore" : userDefinedPackagePrefix;
        packagePrefix = packagePrefix.TrimEnd('.');

        switch (productName)
        {
            case "Sitecore CMS":
                return packagePrefix;

            case "Web Forms for Marketers":
                return packagePrefix + "." + "WFFM";

            case "Email Experience Manager":
                return packagePrefix + "." + "EXM";

            case "Print Experience Manager":
                return packagePrefix + "." + "PXM";

            default:
                // the rest are not supported
                return null;
        }
    }

    public static bool IsKnownPackage(string productName)
    {
        if (productName == "Sitecore CMS" ||
            productName == "Web Forms for Marketers" ||
            productName == "Email Experience Manager" ||
            productName == "Print Experience Manager")
        {
            return true;
        }

        return false;
    }

    public static string GetFilePrefix(string productName)
    {
      var filePrefix = productName;
      if (productName == "Sitecore CMS")
      {
        filePrefix = "Sitecore";
      }
      return filePrefix;
    }

    [NotNull]
    public virtual IEnumerable<string> Generate([NotNull] string prefix, 
                                                string productName, 
                                                [NotNull] string packageFilePath, 
                                                [NotNull] string releaseVersion, 
                                                [NotNull] string outputFolderPath)
    {
        return Generate(String.Empty, productName, packageFilePath, releaseVersion, outputFolderPath, true);
    }

    [NotNull]
    public virtual IEnumerable<string> Generate(string userDefinedPrefix, 
                                                [NotNull] string productName, 
                                                [NotNull] string packageFilePath, 
                                                [NotNull] string releaseVersion, 
                                                [NotNull] string outputFolderPath,
                                                bool sitecoreOnlyDLLs)
    {
        Assert.ArgumentNotNull(packageFilePath, "packageFilePath");
        Assert.ArgumentNotNull(productName, "productName");
        Assert.ArgumentNotNull(releaseVersion, "releaseVersion");
        Assert.ArgumentNotNull(outputFolderPath, "outputFolderPath");

        var tmp = Path.GetTempFileName() + ".dir";
        try
        {
            Directory.CreateDirectory(tmp);

            using (var zip = new ZipFile(packageFilePath))
            {
                if (zip.Count == 1)
                {
                    var tmp2 = Path.GetTempFileName() + ".dir";
                    try
                    {
                        var packageZip = Path.Combine(tmp2, "package.zip");
                        this.Save(zip, zip.GetEntry("package.zip"), packageZip);

                        using (var innerZip = new ZipFile(packageZip))
                        {
                            foreach (var entry in innerZip.OfType<ZipEntry>())
                            {
                                if (!entry.IsFile || !entry.Name.EndsWith(".dll"))
                                {
                                    continue;
                                }

                                var fileName = Path.GetFileName(entry.Name);
                                var outputFilePath = Path.Combine(tmp, fileName);
                                this.Save(innerZip, entry, outputFilePath);
                            }
                        }
                    }
                    finally
                    {
                        if (Directory.Exists(tmp2))
                        {
                            Directory.Delete(tmp2, true);
                        }
                    }
                }
                else
                {
                    foreach (var entry in zip.OfType<ZipEntry>())
                    {
                        if (!entry.IsFile || !entry.Name.EndsWith(".dll"))
                        {
                            continue;
                        }

                        var fileName = Path.GetFileName(entry.Name);
                        var outputFilePath = Path.Combine(tmp, fileName);
                        this.Save(zip, entry, outputFilePath);
                    }
                }
            }

            var enumerable = this.GeneratePackages(userDefinedPrefix, productName, tmp, outputFolderPath, releaseVersion, Path.GetFileNameWithoutExtension(packageFilePath), sitecoreOnlyDLLs);

            // to prevent finally { ... } running before enumeration starts
            return enumerable.ToArray();
        }
        finally
        {
            if (Directory.Exists(tmp))
            {
                Directory.Delete(tmp, true);
            }
        }
    }

    [NotNull]
    public virtual IEnumerable<string> GeneratePackages(string userDefinedPrefix,
                                                        [NotNull] string productName,
                                                        [NotNull] string binFolderPath, 
                                                        [NotNull] string outputFolderPath, 
                                                        [NotNull] string releaseVersion,
                                                        [NotNull] string releaseTitle,
                                                        [NotNull] bool sitecoreOnlyDLLs = true)
    {
      Assert.ArgumentNotNull(productName, "productName");
      Assert.ArgumentNotNull(binFolderPath, "binFolderPath");
      Assert.ArgumentNotNull(outputFolderPath, "outputFolderPath");
      Assert.ArgumentNotNull(releaseVersion, "releaseVersion");
      Assert.ArgumentNotNull(releaseTitle, "releaseTitle");
      Assert.ArgumentNotNull(sitecoreOnlyDLLs, "sitecoreOnlyDLLs");
      
      var finalPackages = new List<string>();
      var binFolder = new DirectoryInfo(binFolderPath);
      Assert.IsTrue(binFolder.Exists, "The folder does not exist: {0}", binFolder.FullName);

      string fileFilterString = sitecoreOnlyDLLs ? "Sitecore.*.dll" : "*.dll";
      var files = binFolder.GetFiles(fileFilterString, SearchOption.AllDirectories);

      var releaseFolderPath = Path.Combine(outputFolderPath, productName + "." + releaseVersion);
      if (!Directory.Exists(releaseFolderPath))
      {
        Directory.CreateDirectory(releaseFolderPath);
      }

      var singleDLLPackages = new List<string>();
      foreach (var file in files)
      {
        if (file == null)
        {
          continue;
        }
        singleDLLPackages.Add(this.GenerateSingleDLLPackage(userDefinedPrefix, file.FullName, releaseFolderPath, releaseVersion, releaseTitle));
      }    
      
      finalPackages.AddRange(singleDLLPackages);

      // Do custom packages only for Sitecore zips.
      if (productName == "Sitecore CMS")
      {
        var customDefinitionPackages = GenerateCustomPackages(userDefinedPrefix, binFolderPath, releaseFolderPath, releaseVersion, singleDLLPackages);
        finalPackages.AddRange(customDefinitionPackages);
      }

      finalPackages.Add(GenerateMainPackage(userDefinedPrefix, productName, files, releaseFolderPath, releaseVersion, releaseTitle));

      finalPackages.Add(GenerateLinksPackage(userDefinedPrefix, productName, files, releaseFolderPath, releaseVersion, releaseTitle));

      finalPackages.Add(GenerateFilesPackage(userDefinedPrefix, productName, binFolder.GetFiles("*.dll", SearchOption.AllDirectories), releaseFolderPath, releaseVersion, releaseTitle));

      return finalPackages;
    }

    private static IEnumerable<string> GenerateCustomPackages(string userDefinedPrefix,
                                                              string binFolderPath, 
                                                              string outputFolderPath, 
                                                              string releaseVersion, 
                                                              IEnumerable<string> singleDLLPackages)
    {
      // Generate Custom Packages defined in the PackageDefinition config
      var customDefinitionPackages = new List<string>();
      var customPackageGenerator = new CustomPackageGenerator();
      foreach (PackageDefinition definition in PackageDefinition.PackageDefinitions)
      {
          var customPackageCreated = customPackageGenerator.GenerateSingleCustomPackage(definition, 
                                                                                        binFolderPath, 
                                                                                        outputFolderPath, 
                                                                                        releaseVersion, 
                                                                                        singleDLLPackages,
                                                                                        customDefinitionPackages,
                                                                                        userDefinedPrefix);
          if (!String.IsNullOrEmpty(customPackageCreated))
          {
              customDefinitionPackages.Add(customPackageCreated);
          }
      }

      return customDefinitionPackages;
    }

    [NotNull]
    private static string GenerateFilesPackage(string userDefinedPrefix,
                                               [NotNull] string productName,
                                               [NotNull] IEnumerable<FileInfo> files, 
                                               [NotNull] string releaseFolderPath, 
                                               [NotNull] string releaseVersion, 
                                               [NotNull] string releaseTitle)
    {
       Assert.ArgumentNotNull(files, "files");
       Assert.ArgumentNotNull(releaseFolderPath, "releaseFolderPath");
       Assert.ArgumentNotNull(releaseVersion, "releaseVersion");
       Assert.ArgumentNotNull(releaseTitle, "releaseTitle");

       string packageName = GetAbbr(productName, userDefinedPrefix) + "-AsFiles";

       var metadata = new ManifestMetadata
       {
         Id = packageName,
         Version = releaseVersion,
         Authors = "Sitecore",
         Description = "All assemblies of Sitecore " + releaseTitle + " as content files in /App_Data/bin folder",
         RequireLicenseAcceptance = false
       };
 
       var builder = new PackageBuilder();
 
       foreach (var assembly in files)
       {
         var assemblyFilePath = assembly.FullName;
         builder.Files.Add(new PhysicalPackageFile
         {
           SourcePath = assemblyFilePath,
           TargetPath = "content\\App_Bin\\" + Path.GetFileName(assemblyFilePath)
         });
       }
 
       builder.Populate(metadata);
 
       var nugetFilePath = Path.Combine(releaseFolderPath, metadata.Id + "." + releaseVersion + ".nupkg");
       using (FileStream stream = File.Open(nugetFilePath, FileMode.OpenOrCreate))
       {
         builder.Save(stream);
       }
 
       return nugetFilePath;
     }

     [NotNull]
     private static string GenerateLinksPackage(string userDefinedPrefix,
                                                [NotNull] string productName,
                                                [NotNull] IEnumerable<FileInfo> files, 
                                                [NotNull] string releaseFolderPath, 
                                                [NotNull] string releaseVersion, 
                                                [NotNull] string releaseTitle)
     {
       Assert.ArgumentNotNull(productName, "productName");
       Assert.ArgumentNotNull(files, "files");
       Assert.ArgumentNotNull(releaseFolderPath, "releaseFolderPath");
       Assert.ArgumentNotNull(releaseVersion, "releaseVersion");
       Assert.ArgumentNotNull(releaseTitle, "releaseTitle");
 
       var installPs1 = Path.GetTempFileName();
       var uninstallPs1 = Path.GetTempFileName();
       try
       {
         File.WriteAllText(installPs1, InstallPs1);
         File.WriteAllText(uninstallPs1, UninstallPs1);

         string packageName = GetAbbr(productName, userDefinedPrefix) + "-AsLinks";
 
         var metadata = new ManifestMetadata
         {
           Id = packageName,
           Version = releaseVersion,
           Authors = "Sitecore",
           Description = "All assemblies of Sitecore " + releaseTitle + " as links to ../packages/SC.{ver}/*.dll in /App_Data/bin folder",
           RequireLicenseAcceptance = false
         };
 
         var builder = new PackageBuilder();
 
         foreach (var assembly in files)
         {
           var assemblyFilePath = assembly.FullName;
           builder.Files.Add(new PhysicalPackageFile
           {
             SourcePath = assemblyFilePath,
             TargetPath = "App_Bin\\" + Path.GetFileName(assemblyFilePath)
           });
         }
 
         builder.Files.Add(new PhysicalPackageFile
         {
           SourcePath = installPs1,
           TargetPath = "tools\\install.ps1"
         });
 
         builder.Files.Add(new PhysicalPackageFile
         {
           SourcePath = uninstallPs1,
           TargetPath = "tools\\uninstall.ps1"
         });
 
         builder.Populate(metadata);
 
         var nugetFilePath = Path.Combine(releaseFolderPath, metadata.Id + "." + releaseVersion + ".nupkg");
         using (FileStream stream = File.Open(nugetFilePath, FileMode.OpenOrCreate))
         {
           builder.Save(stream);
         }
 
         return nugetFilePath;
       }
       finally
       {
         if (File.Exists(installPs1))
         {
           File.Delete(installPs1);
         }
 
         if (File.Exists(uninstallPs1))
         {
           File.Delete(uninstallPs1);
         }
       }
     }

    [NotNull]
    private static string GenerateMainPackage(string userDefinedPrefix,
                                              [NotNull] string productName,
                                              [NotNull] IEnumerable<FileInfo> files, 
                                              [NotNull] string releaseFolderPath, 
                                              [NotNull] string releaseVersion, 
                                              [NotNull] string releaseTitle)
    {
      Assert.ArgumentNotNull(files, "files");
      Assert.ArgumentNotNull(releaseFolderPath, "releaseFolderPath");
      Assert.ArgumentNotNull(releaseVersion, "releaseVersion");
      Assert.ArgumentNotNull(releaseTitle, "releaseTitle");
      
      var dependencies = files.Select(x => new ManifestDependency { Id = GetNuGetName(x.FullName, userDefinedPrefix), Version = releaseVersion });

      string packageName = GetAbbr(productName, userDefinedPrefix).TrimEnd('.');

      var metadata = new ManifestMetadata
      {
        Id = packageName,
        Version = releaseVersion,
        Authors = "Sitecore",
        Description = "All assemblies of Sitecore " + releaseTitle,
        RequireLicenseAcceptance = false,
        DependencySets = new List<ManifestDependencySet>
        {
          new ManifestDependencySet
          {
            Dependencies = dependencies.ToList()
          }
        }
      };

      var builder = new PackageBuilder();

      builder.Populate(metadata);
      var nugetFilePath = Path.Combine(releaseFolderPath, metadata.Id + "." + releaseVersion + ".nupkg");
      using (FileStream stream = File.Open(nugetFilePath, FileMode.OpenOrCreate))
      {
        builder.Save(stream);
      }

      return nugetFilePath;
    }

    [NotNull]
    public virtual string GenerateSingleDLLPackage(string userDefinedPrefix,
                                                   [NotNull] string assemblyFilePath, 
                                                   [NotNull] string releaseFolderPath, 
                                                   [NotNull] string releaseVersion, 
                                                   [NotNull] string releaseTitle)
    {
      Assert.ArgumentNotNull(assemblyFilePath, "assemblyFilePath");
      Assert.ArgumentNotNull(releaseFolderPath, "releaseFolderPath");
      Assert.ArgumentNotNull(releaseVersion, "releaseVersion");
      Assert.ArgumentNotNull(releaseTitle, "releaseTitle");

      var version = FileVersionInfo.GetVersionInfo(assemblyFilePath);
      var nugetName = GetNuGetName(assemblyFilePath, userDefinedPrefix);

      var metadata = new ManifestMetadata
      {
        Id = nugetName + ".NoReferences",
        Version = releaseVersion,
        Authors = "Sitecore",
        RequireLicenseAcceptance = false,
        Description = string.Format("{0} assembly of {1}, assembly file version: {2}, product version: {3}", nugetName, releaseTitle, version.FileVersion, version.ProductVersion),
      };

      var builder = new PackageBuilder();

      builder.Files.Add(new PhysicalPackageFile
      {
        SourcePath = assemblyFilePath,
        TargetPath = "lib\\" + Path.GetFileName(assemblyFilePath)
      });

      var copyLocalInstallPs1 = Path.GetTempFileName();
      File.WriteAllText(copyLocalInstallPs1, CopyLocalInstallPs1);
      builder.Files.Add(new PhysicalPackageFile
      {
        SourcePath = copyLocalInstallPs1,
        TargetPath = "tools\\install.ps1"
      });

      builder.Populate(metadata);

      var nugetFilePath = Path.Combine(releaseFolderPath, metadata.Id + "." + releaseVersion + ".nupkg");
      this.Save(builder, nugetFilePath);

      return nugetFilePath;
    }

    protected virtual void Save([NotNull] PackageBuilder builder, [NotNull] string nugetFilePath)
    {
      Assert.ArgumentNotNull(builder, "builder");
      Assert.ArgumentNotNull(nugetFilePath, "nugetFilePath");

      using (var stream = File.Open(nugetFilePath, FileMode.OpenOrCreate))
      {
        builder.Save(stream);
      }
    }

    protected virtual void Save([NotNull] ZipFile zip, [NotNull] ZipEntry entry, [NotNull] string outputFilePath)
    {
      Assert.ArgumentNotNull(zip, "zip");
      Assert.ArgumentNotNull(entry, "entry");
      Assert.ArgumentNotNull(outputFilePath, "outputFilePath");

      using (var input = zip.GetInputStream(entry))
      {
        var folderPath = Path.GetDirectoryName(outputFilePath);
        if (!Directory.Exists(folderPath))
        {
          Directory.CreateDirectory(folderPath);
        }

        using (Stream output = File.OpenWrite(outputFilePath))
        {
          this.CopyStream(input, output);
        }
      }
    }

    protected virtual void CopyStream([NotNull] Stream input, [NotNull] Stream output)
    {
      Assert.ArgumentNotNull(input, "input");
      Assert.ArgumentNotNull(output, "output");

      var buffer = new byte[8 * 1024];
      int len;
      while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
      {
        output.Write(buffer, 0, len);
      }
    }

    [NotNull]
    private static string GetNuGetName([NotNull] string assemblyFilePath, string packageNamePrefix)
    {
      Assert.ArgumentNotNull(assemblyFilePath, "assemblyFilePath");

      return packageNamePrefix + Path.GetFileNameWithoutExtension(assemblyFilePath);
    }


  }
}
