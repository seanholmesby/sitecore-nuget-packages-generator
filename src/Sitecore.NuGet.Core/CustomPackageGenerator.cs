﻿using NuGet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Sitecore.NuGet.Core
{
    /// <summary>
    /// Package Creator for Custom Packages, defined in the PackageDefinition class.
    /// These packages are generated with only dependencies on single-DLL packages.
    /// </summary>
    public class CustomPackageGenerator
    {
        public string GenerateSingleCustomPackage(PackageDefinition definition,
                                                  string binFolderPath, 
                                                  string outputFolderPath, 
                                                  string releaseVersion, 
                                                  IEnumerable<string> singleDLLPackages,
                                                  IEnumerable<string> customPackages,
                                                  string packageNamePrefix)
        {
            // if no dependencies exist, don't create package from definition
            IEnumerable<ManifestDependency> dependencies = this.GenerateDependencies(definition.Dependencies, 
                                                                                     binFolderPath,
                                                                                     releaseVersion, 
                                                                                     singleDLLPackages, 
                                                                                     customPackages,
                                                                                     packageNamePrefix);
            if (!dependencies.Any())
            {
                return String.Empty;
            }

            // Create nupkg file
            var packageId = packageNamePrefix + definition.Id + "." + releaseVersion;
            var nugetFilePath = Path.Combine(outputFolderPath, packageId + ".nupkg");

            var metadata = new ManifestMetadata
            {
                Id = packageNamePrefix + definition.Id,
                Title = packageNamePrefix + definition.Id,
                Version = releaseVersion,
                Tags = definition.Tag,
                Authors = "Sitecore",
                Owners = "Sitecore",
                Description = definition.Description,
                RequireLicenseAcceptance = false,
                IconUrl = "http://www.sitecore.net/favicon.ico",
                DependencySets = new List<ManifestDependencySet>
                {
                  new ManifestDependencySet
                  {
                        Dependencies =  dependencies.ToList()
                  }
                }
            };

            var builder = new PackageBuilder();
            builder.Populate(metadata);
            using (FileStream stream = File.Open(nugetFilePath, FileMode.OpenOrCreate))
            {
                builder.Save(stream);
            }

            return nugetFilePath;
        }

        private IEnumerable<ManifestDependency> GenerateDependencies(IEnumerable<string> specifiedDependencies, 
                                                                     string binFolderPath,
                                                                     string sitecoreReleaseVersion, 
                                                                     IEnumerable<string> singleDLLPackages,
                                                                     IEnumerable<string> customPackages,
                                                                     string packageNamePrefix)
        {
            var confirmedDependencies = new List<ManifestDependency>();
            var packageDiscoverer = new ThirdPartyPackageDiscoverer();
            foreach (string dependencyPackage in specifiedDependencies)
            {
                // check if the dependency starts with 'Sitecore', does it exist in the singleDLLpackages 
                // i.e is it a Sitecore package and does it exist in this Sitecore version?
                if (dependencyPackage.StartsWith("Sitecore"))
                {

                    if (singleDLLPackages.Any(x => x.EndsWith(dependencyPackage + "." + sitecoreReleaseVersion + ".nupkg")) ||
                        customPackages.Any(x => x.EndsWith(dependencyPackage + "." + sitecoreReleaseVersion + ".nupkg")))
                    {
                        confirmedDependencies.Add(new ManifestDependency() { Id = packageNamePrefix + dependencyPackage, Version = sitecoreReleaseVersion });
                    }
                }
                else
                {
                    bool isMvcPackage = dependencyPackage == "Microsoft.AspNet.Mvc";
                    bool isWebApiPackage = dependencyPackage == "Microsoft.AspNet.WebApi";

                    bool isSitecoreZipWithoutMvcDLLs = sitecoreReleaseVersion.StartsWith("7.1") ||
                                                       sitecoreReleaseVersion.StartsWith("7.0") ||
                                                       sitecoreReleaseVersion.StartsWith("6");

                    bool isSitecoreZipWithoutWebApiDLLs = sitecoreReleaseVersion.StartsWith("7.2") ||
                                                          sitecoreReleaseVersion.StartsWith("7.1") ||
                                                          sitecoreReleaseVersion.StartsWith("7.0") ||
                                                          sitecoreReleaseVersion.StartsWith("6");
                    
                    // if you're using Sitecore 5 or earlier, a Nuget package generator is the last thing you need to worry about

                    ManifestDependency publicNugetPackage = null;
                    if (isWebApiPackage && isSitecoreZipWithoutWebApiDLLs)
                    {
                        publicNugetPackage = new ManifestDependency() { Id = dependencyPackage, Version = "4.0.30506" };
                    }
                    else if (isMvcPackage && isSitecoreZipWithoutMvcDLLs)
                    {
                        if (sitecoreReleaseVersion.StartsWith("7.1"))
                        {
                            publicNugetPackage = new ManifestDependency() { Id = dependencyPackage, Version = "4.0.30506" };
                        }
                        else // if (sitecoreReleaseVersion.StartsWith("7.0") || sitecoreReleaseVersion.StartsWith("6"))
                        {
                            publicNugetPackage = new ManifestDependency() { Id = dependencyPackage, Version = "3.0.20105.1" };
                        }
                    }
                    else
                    {
                        // check Public Nuget Repo for packages
                        publicNugetPackage = packageDiscoverer.FindPublicThirdPartyNugetPackage(dependencyPackage, binFolderPath);
                    }

                    if (publicNugetPackage != null)
                    {
                        confirmedDependencies.Add(publicNugetPackage);
                    }
                }
            }

            return confirmedDependencies;
        }

    }
}
