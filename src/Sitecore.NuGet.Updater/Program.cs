﻿namespace Sitecore.NuGet.Updater
{
  using System;
  using System.Diagnostics;
  using System.IO;
  using System.Linq;
  using System.Text.RegularExpressions;
  using Sitecore.Diagnostics.Base;
  using Sitecore.Diagnostics.InformationService.Client;
  using Sitecore.NuGet.Core;

  public static class Program
  {
    public static readonly Regex ProductFileNameRegex = new Regex(@"\s*(\d)\.(\d)\.?(\d?)\s*rev\.\s*(\d\d\d\d\d\d)", RegexOptions.Compiled);

    private static int Main(string[] args)
    {
      if (args == null || (args.Length < 2 || args.Length > 5))
      {
        Console.WriteLine("The application expects between two and five arguments:");
        Console.WriteLine("1. The path to the specific zip distributive or to the folder with them");
        Console.WriteLine("2. The output folder");
        Console.WriteLine("3. (Optional) Generate only packages for Sitecore DLLs, not third party DLLs. (format: true, false) (default: true)");
        Console.WriteLine("4. (Optional) Prefix for the names of the output Nuget Packages (default: none)");
        Console.WriteLine("5. (Optional) NuGet Server credentials and address to push package to (format: user:pass@servername)");
        
        return -1;
      }

      Process(
        args.Skip(0).First(),
        args.Skip(1).First(),
        args.Skip(2).FirstOrDefault(),
        args.Skip(3).FirstOrDefault(),
        args.Skip(4).FirstOrDefault()
        );

      return 0;
    }

    private static void Process(string p1, string p2, string p3, string p4, string p5)
    {
      if (!Directory.Exists(p2))
      {
        Directory.CreateDirectory(p2);
      }

      var sitecoreOnlyDLLs = String.IsNullOrWhiteSpace(p3) ? true : Boolean.Parse(p3);
      var pushData = ServerInfo.Parse(p5);
      if (Directory.Exists(p1))
      {
        ProcessFolder(p1, p2, sitecoreOnlyDLLs, p4, pushData);
      }
      else
      {
        ProcessFile(p1, p2, sitecoreOnlyDLLs, p4, pushData);
      }
    }

    private static void ProcessFolder(string directory, string outputFolderPath, bool sitecoreOnlyDLLs, string userDefinedPrefix, ServerInfo pushData)
    {
       var client = new ServiceClient();
       foreach (var productName in client.GetProductNames())
       {
           var filePrefix = PackageGenerator.GetFilePrefix(productName);
           var knownPackage = PackageGenerator.IsKnownPackage(productName);
           if (!knownPackage)
           {
               continue;
           }

           foreach (var version in client.GetVersions(productName))
           {
               var majorMinor = PackageGenerator.GetMajorMinor(version);

               foreach (var release in version.Releases)
               {
                   var revision = release.Revision;
                   var pattern = filePrefix + " " + majorMinor + "* rev. " + revision + ".zip";

                   var zipfiles = Directory.GetFiles(directory, pattern, SearchOption.AllDirectories);
                   if (zipfiles.Length <= 0)
                   {
                       continue;
                   }

                   var releaseVersion = PackageGenerator.GetReleaseVersion(majorMinor, revision);
                   var file = zipfiles.First();
                   try
                   {
                       Console.WriteLine("File: " + file);

                       // Create nupkg file
                       ProcessFile(file, outputFolderPath, sitecoreOnlyDLLs, userDefinedPrefix, pushData);
                   }
                   catch (Exception ex)
                   {
                       Console.WriteLine("Error processing file " + file + ". " + ex.Message + Environment.NewLine + "Stack trace:" + Environment.NewLine + ex.StackTrace);
                   }
               }
           }

       }
    }

    private static void ProcessFile(string file, string outputFolderPath, bool sitecoreOnlyDLLs, string userDefinedPrefix, ServerInfo pushData)
    {
        string filename = Path.GetFileName(file);
      Console.WriteLine("File: " + filename);
      var match = ProductFileNameRegex.Match(filename);
      if (!match.Success)
      {
        Console.WriteLine("Skipped (regex)");
      }
      else
      {
        var v = VersionInfo.Parse(match);
        if (v == null || v.Major < 0)
        {
          return;
        }
        
        // Create nupkg file
        var nupkgFiles = new PackageGenerator().Generate(file, outputFolderPath, userDefinedPrefix, sitecoreOnlyDLLs);

        // Publish nupkg files
        foreach (var nupkgFile in nupkgFiles)
        {
          PublishPackage(nupkgFile, pushData);
        }
      }
    }

    private static void PublishPackage(string nupkgPath, ServerInfo pushData)
    {
      if (pushData == null)
      {
        return;
      }

      Console.WriteLine("Pushing to the server");

      var source = GetSource(pushData);
      var credentials = pushData.Credentials;
      var arguments = string.Format("push \"{0}\" -Source {1} -apikey {2}", nupkgPath, source, credentials);
      var processStartInfo = CreateNuGetProcessStartInfo(arguments);
      var process = System.Diagnostics.Process.Start(processStartInfo);

      // wait for end
      process.WaitForExit();
    }

    private static string GetSource(ServerInfo pushData)
    {
      const string Protocol = "://";

      // add a default http protocol if omitted
      var server = pushData.Server;
      if (!server.Contains(Protocol))
      {
        server = "http" + Protocol + server;
      }

      // add a default /nuget/Default feed if omitted
      var position = server.IndexOf(Protocol) + Protocol.Length;
      if (server.TrimEnd('/').IndexOf("/", position) < 0)
      {
        server = server + "/nuget/Default";
      }

      return server;
    }

    private static ProcessStartInfo CreateNuGetProcessStartInfo(string arguments)
    {
      var nugetExeFilePath = Path.Combine(Environment.CurrentDirectory, "NuGet27.exe");
      var processStartInfo = new ProcessStartInfo(nugetExeFilePath, arguments)
      {
        CreateNoWindow = true,
        WindowStyle = ProcessWindowStyle.Hidden
      };
      return processStartInfo;
    }
  }
}